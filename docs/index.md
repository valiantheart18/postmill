# ![](assets/postmill-128.png) Postmill documentation

1. **[Getting started](../README.md#getting-started)**

2. **Configuration & administration**

    * [Using Docker](docker.md)
    * [Database setup](database-setup.md)
    * [Configuring a web server][web server] (external link)
    * [Overriding templates & translations](overrides.md)
    * [Pruning IP addresses automatically](pruning-ips.md)
    * [How to upgrade Postmill](upgrading.md)
    * [Deployment][deploy] (external link)

3. **Reference**

    * [Deprecated CSS classes](deprecated-css-classes.md)
    * [Webhooks](webhooks.md)

4. **For developers**
    * [About the unit tests](about-the-unit-tests.md)
    * [Ugly hacks & workarounds](workarounds.md)
    * [Trusted users](trusted_users.md)

5. **Appendix**
    * [Version number policy](version-policy.md)

Something missing? Report it on the [issue tracker][issues] and chances are
we'll add it!


[deploy]: https://symfony.com/doc/4.1/deployment.html
[web server]: https://symfony.com/doc/4.1/setup/web_server_configuration.html
[issues]: https://gitlab.com/edgyemma/Postmill/issues
